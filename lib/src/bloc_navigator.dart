import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class BlocNavigator<B extends BlocBase<S>, S, NS>
    extends StatelessWidget {
  final NS Function(S state) _buildNavigatorState;
  final List<Page> Function(BuildContext context, NS navigatorState) _pages;
  final void Function(BuildContext context, NS navigatorState, dynamic result)?
      _onPop;

  BlocNavigator({
    required NS Function(S state) buildNavigatorState,
    required List<Page> Function(BuildContext context, NS navigatorState) pages,
    void Function(BuildContext context, NS navigatorState, dynamic result)?
        onPop,
  })  : _buildNavigatorState = buildNavigatorState,
        _pages = pages,
        _onPop = onPop;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<B, S>(
      buildWhen: (previous, current) {
        final previousNavigatorState = _buildNavigatorState(previous);
        final currentNavigatorState = _buildNavigatorState(current);
        if (previousNavigatorState != currentNavigatorState) {
          return true;
        }
        return false;
      },
      builder: (context, state) {
        return Navigator(
          pages: _pages(context, _buildNavigatorState(state)),
          onPopPage: (route, result) {
            if (_onPop != null) {
              _onPop!(context, _buildNavigatorState(state), result);
            }
            return route.didPop(result);
          },
        );
      },
    );
  }
}
