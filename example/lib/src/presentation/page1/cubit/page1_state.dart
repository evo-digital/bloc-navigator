import 'page1_navigator_state.dart';

class Page1State {
  final Page1NavigatorState navigatorState;

  Page1State({Page1NavigatorState? navigatorState})
      : this.navigatorState = navigatorState ?? Page1NavigatorState.initial;

  Page1State copyWith({Page1NavigatorState? navigatorState}) {
    return Page1State(
      navigatorState: navigatorState ?? this.navigatorState,
    );
  }
}
