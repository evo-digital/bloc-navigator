import 'package:flutter_bloc/flutter_bloc.dart';

import 'page1_state.dart';
import 'page1_navigator_state.dart';

class Page1Cubit extends Cubit<Page1State> {
  Page1Cubit() : super(Page1State());

  void onPage2ButtonTap() {
    emit(state.copyWith(navigatorState: Page1NavigatorState.page2));
  }

  void onPop() {
    emit(state.copyWith(navigatorState: Page1NavigatorState.initial));
  }
}
