import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit/cubit.dart';

class Page1Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page 1'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: BlocProvider.of<Page1Cubit>(context).onPage2ButtonTap,
          child: Text('Push replacement to Page 2'),
        ),
      ),
    );
  }
}
