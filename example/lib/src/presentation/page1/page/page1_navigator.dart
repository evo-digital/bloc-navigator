import 'package:bloc_navigator/bloc_navigator.dart';
import 'package:example/src/presentation/page2/page/page.dart';
import 'package:flutter/material.dart';

import 'page1_widget.dart';
import '../cubit/cubit.dart';

class Page1Navigator
    extends BlocNavigator<Page1Cubit, Page1State, Page1NavigatorState> {
  Page1Navigator()
      : super(
          buildNavigatorState: (state) => state.navigatorState,
          pages: (context, navigatorState) => [
            if (navigatorState == Page1NavigatorState.initial)
              MaterialPage(child: Page1Widget()),
            if (navigatorState == Page1NavigatorState.page2) Page2(),
          ],
        );
}
