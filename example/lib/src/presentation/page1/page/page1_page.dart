import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'page1_navigator.dart';
import '../cubit/cubit.dart';

class Page1 extends MaterialPage {
  Page1()
      : super(
          child: BlocProvider(
            create: (context) => Page1Cubit(),
            child: Page1Navigator(),
          ),
        );
}
