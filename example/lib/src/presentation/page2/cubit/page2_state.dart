import 'page2_navigator_state.dart';

class Page2State {
  final Page2NavigatorState navigatorState;

  Page2State({Page2NavigatorState? navigatorState})
      : this.navigatorState = navigatorState ?? Page2NavigatorState.initial;

  Page2State copyWith({Page2NavigatorState? navigatorState}) {
    return Page2State(
      navigatorState: navigatorState ?? this.navigatorState,
    );
  }
}
