import 'package:flutter_bloc/flutter_bloc.dart';

import 'page2_state.dart';
import 'page2_navigator_state.dart';

class Page2Cubit extends Cubit<Page2State> {
  final void Function() _popReplacement;

  Page2Cubit({required void Function() onPop})
      : _popReplacement = onPop,
        super(Page2State());

  void onPage1ButtonTap() {
    _popReplacement();
  }

  void onPage3ButtonTap() {
    emit(state.copyWith(navigatorState: Page2NavigatorState.page3));
  }

  void onPop() {
    emit(state.copyWith(navigatorState: Page2NavigatorState.initial));
  }
}
