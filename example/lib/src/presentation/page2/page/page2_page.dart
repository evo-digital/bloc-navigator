import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'page2_navigator.dart';
import '../cubit/cubit.dart';
import 'package:example/src/presentation/page1/page1.dart';

class Page2 extends MaterialPage {
  Page2()
      : super(
          child: BlocProvider(
            create: (context) => Page2Cubit(
              onPop: BlocProvider.of<Page1Cubit>(context).onPop,
            ),
            child: Page2Navigator(),
          ),
        );
}
