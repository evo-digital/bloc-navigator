import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit/cubit.dart';

class Page2Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page 2'),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ElevatedButton(
              onPressed: BlocProvider.of<Page2Cubit>(context).onPage1ButtonTap,
              child: Text('Back replacement to Page 1'),
            ),
            SizedBox(height: 12),
            ElevatedButton(
              onPressed: BlocProvider.of<Page2Cubit>(context).onPage3ButtonTap,
              child: Text('Push to Page 3'),
            ),
          ],
        ),
      ),
    );
  }
}
