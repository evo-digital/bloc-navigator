import 'package:bloc_navigator/bloc_navigator.dart';
import 'package:example/src/presentation/page3/page/page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'page2_widget.dart';
import '../cubit/cubit.dart';

class Page2Navigator
    extends BlocNavigator<Page2Cubit, Page2State, Page2NavigatorState> {
  Page2Navigator()
      : super(
          buildNavigatorState: (state) => state.navigatorState,
          pages: (context, navigatorState) => [
            MaterialPage(child: Page2Widget()),
            if (navigatorState == Page2NavigatorState.page3) Page3(),
          ],
          onPop: (context, navigatorState, result) {
            if (navigatorState == Page2NavigatorState.page3) {
              BlocProvider.of<Page2Cubit>(context).onPop();
            }
          },
        );
}
