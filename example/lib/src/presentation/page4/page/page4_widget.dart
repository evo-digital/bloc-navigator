import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit/cubit.dart';

class Page4Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          onPressed: BlocProvider.of<Page4Cubit>(context).onBackButtonTap,
        ),
        title: Text('Page 4'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: BlocProvider.of<Page4Cubit>(context).onBackRootButtonTap,
          child: Text('Pop to root (Page 1)'),
        ),
      ),
    );
  }
}
