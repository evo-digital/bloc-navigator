import 'package:example/src/presentation/page1/page1.dart';
import 'package:example/src/presentation/page3/cubit/cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'page4_navigator.dart';
import '../cubit/cubit.dart';

class Page4 extends MaterialPage {
  Page4()
      : super(
          child: BlocProvider(
            create: (context) => Page4Cubit(
              pop: BlocProvider.of<Page3Cubit>(context).onPop,
              popRoot: BlocProvider.of<Page1Cubit>(context).onPop,
            ),
            child: Page4Navigator(),
          ),
        );
}
