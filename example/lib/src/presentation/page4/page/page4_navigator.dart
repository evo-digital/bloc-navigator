import 'package:bloc_navigator/bloc_navigator.dart';
import 'package:flutter/material.dart';

import 'page4_widget.dart';
import '../cubit/cubit.dart';

class Page4Navigator
    extends BlocNavigator<Page4Cubit, Page4State, Page4NavigatorState> {
  Page4Navigator()
      : super(
          buildNavigatorState: (state) => state.navigatorState,
          pages: (context, navigatorState) => [
            MaterialPage(child: Page4Widget()),
          ],
        );
}
