import 'page4_navigator_state.dart';

class Page4State {
  final Page4NavigatorState navigatorState;

  Page4State({Page4NavigatorState? navigatorState})
      : this.navigatorState = navigatorState ?? Page4NavigatorState.initial;

  Page4State copyWith({Page4NavigatorState? navigatorState}) {
    return Page4State(
      navigatorState: navigatorState ?? this.navigatorState,
    );
  }
}
