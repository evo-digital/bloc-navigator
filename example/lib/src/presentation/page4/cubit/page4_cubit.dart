import 'package:flutter_bloc/flutter_bloc.dart';

import 'page4_state.dart';
import 'page4_navigator_state.dart';

class Page4Cubit extends Cubit<Page4State> {
  final void Function() _pop;
  final void Function() _popRoot;

  Page4Cubit({required void Function() pop, required void Function() popRoot})
      : _pop = pop,
        _popRoot = popRoot,
        super(Page4State());

  void onBackButtonTap() {
    _pop();
  }

  void onBackRootButtonTap() {
    _popRoot();
  }
}
