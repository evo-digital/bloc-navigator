import 'package:flutter_bloc/flutter_bloc.dart';

import 'page3_state.dart';
import 'page3_navigator_state.dart';

class Page3Cubit extends Cubit<Page3State> {
  final void Function() _pop;

  Page3Cubit({required void Function() pop})
      : _pop = pop,
        super(Page3State());

  void onBackButtonTap() {
    _pop();
  }

  void onPage4ButtonTap() {
    emit(state.copyWith(navigatorState: Page3NavigatorState.page4));
  }

  void onPop() {
    emit(state.copyWith(navigatorState: Page3NavigatorState.initial));
  }
}
