import 'page3_navigator_state.dart';

class Page3State {
  final Page3NavigatorState navigatorState;

  Page3State({Page3NavigatorState? navigatorState})
      : this.navigatorState = navigatorState ?? Page3NavigatorState.initial;

  Page3State copyWith({Page3NavigatorState? navigatorState}) {
    return Page3State(
      navigatorState: navigatorState ?? this.navigatorState,
    );
  }
}
