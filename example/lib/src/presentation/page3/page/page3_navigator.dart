import 'package:bloc_navigator/bloc_navigator.dart';
import 'package:example/src/presentation/page4/page/page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'page3_widget.dart';
import '../cubit/cubit.dart';

class Page3Navigator
    extends BlocNavigator<Page3Cubit, Page3State, Page3NavigatorState> {
  Page3Navigator()
      : super(
          buildNavigatorState: (state) => state.navigatorState,
          pages: (context, navigatorState) => [
            MaterialPage(child: Page3Widget()),
            if (navigatorState == Page3NavigatorState.page4) Page4(),
          ],
          onPop: (context, navigatorState, result) {
            if (navigatorState == Page3NavigatorState.page4) {
              BlocProvider.of<Page3Cubit>(context).onPop();
            }
          },
        );
}
