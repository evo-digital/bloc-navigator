import 'package:example/src/presentation/page2/cubit/cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'page3_navigator.dart';
import '../cubit/cubit.dart';

class Page3 extends MaterialPage {
  Page3()
      : super(
          child: BlocProvider(
            create: (context) => Page3Cubit(
              pop: BlocProvider.of<Page2Cubit>(context).onPop,
            ),
            child: Page3Navigator(),
          ),
        );
}
