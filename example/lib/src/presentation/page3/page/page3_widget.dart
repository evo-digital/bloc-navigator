import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit/cubit.dart';

class Page3Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          onPressed: BlocProvider.of<Page3Cubit>(context).onBackButtonTap,
        ),
        title: Text('Page 3'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: BlocProvider.of<Page3Cubit>(context).onPage4ButtonTap,
          child: Text('Push to Page 4'),
        ),
      ),
    );
  }
}
