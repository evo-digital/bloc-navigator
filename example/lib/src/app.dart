import 'package:example/src/presentation/page1/page1.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: (_) => Page1().createRoute(context),
    );
  }
}
